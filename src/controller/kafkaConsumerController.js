const { consumer } = require('../config/kafkaClient')
const notifyService = require('../service/notifyService')

const handleCommit = (error, data) => {
  if (error) {
    console.error(error)
  }
  console.log('commit', data)
}

consumer.on('message', async (message) => {
  switch (message.key) {
    case 'notify': {
      const data = JSON.parse(message.value)
      switch (data.channel) {
        case 'sms': {
          await notifyService.notifyToSms(data.to, data.eventName)
          consumer.commit(handleCommit)
          break
        }
        case 'email': {
          await notifyService.notifyToEmail(data.to, data.eventName)
          consumer.commit(handleCommit)
          break
        }
        default: {
          consumer.commit(handleCommit)
        }
      }
      break
    }
    default: {
      consumer.commit(handleCommit)
    }
  }
})

consumer.on('error', (error) => {
  console.error(error)
})
