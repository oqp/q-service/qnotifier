const mongoose = require('mongoose')

const { Schema, SchemaTypes } = mongoose

const { eventSchema } = require('./event')

const waitingRoomSchema = new Schema({
  id: SchemaTypes.ObjectId,
  website: String,
  subdomain: String,
  uuid: String,
  events: [eventSchema],
})

const WaitingRoomModel = mongoose.model('WaitingRoom', waitingRoomSchema)

module.exports = { waitingRoomSchema, WaitingRoomModel }
