const mongoose = require('mongoose')

const { Schema, SchemaTypes } = mongoose

const eventSchema = new Schema({
  id: SchemaTypes.ObjectId,
  uuid: String,
  name: String,
  openQueue: Date,
  closeQueue: Date,
  maxOutflowAmount: Number,
  currentNumber: Number,
  redirectURL: String,
})

const EventModel = mongoose.model('Event', eventSchema)

module.exports = { eventSchema, EventModel }
