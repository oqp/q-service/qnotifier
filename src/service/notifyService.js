require('dotenv').config()

const axios = require('axios')
const qs = require('qs')
const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const twilioSendSms = async (to, eventName) => {
  try {
    await axios.post(
      `${process.env.TWILIO_API_URL}/Accounts/${process.env.TWILIO_ACCOUNT_SID}/Messages`,
      qs.stringify({
        To: to,
        From: process.env.TWILIO_SENDER_NUMBER,
        Body: `Now it's your queue. Go ${eventName}`,
        ForceDelivery: true,
      }),
      {
        headers: {
          contentType: 'application/x-www-form-urlencoded',
        },
        auth: {
          username: process.env.TWILIO_ACCOUNT_SID,
          password: process.env.TWILIO_AUTH_TOKEN,
        },
      },
    )
    return true
  } catch (err) {
    console.error(err)
    return null
  }
}

const sendgridSendEmail = async (to, eventName) => {
  console.log('notify to email provider')
  const msg = {
    to,
    from: 'flukemdc@gmail.com',
    subject: 'It\'s your queue!',
    text: `enter ${eventName} now!`,
    html: `<strong>${eventName}</strong>`,
  }

  try {
    await sgMail.send(msg)
    return true
  } catch (err) {
    return null
  }
}

module.exports = {
  async notifyToEmail(to, eventName) {
    return sendgridSendEmail(to, eventName)
    // return true
  },

  async notifyToBrowser() {
    console.log('notify to browser')
  },

  async notifyToSms(to, eventName) {
    console.log('notify to SMS')
    return twilioSendSms(to, eventName)
    // return true
  },
}
