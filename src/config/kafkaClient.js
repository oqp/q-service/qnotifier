require('dotenv').config({ path: '/' })

const kafka = require('kafka-node')

const client = new kafka.KafkaClient({ kafkaHost: `${process.env.KAFKA_HOST}:${process.env.KAFKA_PORT}` })

const producer = new kafka.Producer(client)

const consumer = new kafka.Consumer(
  client, [
    {
      topic: 'notification',
      partition: 0,
      offset: 0,
    },
  ],
  {
    autoCommit: false,
    fromOffset: false,
  },
)

module.exports = { client, producer, consumer }
