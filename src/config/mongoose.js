require('dotenv').config({ path: '/' })

const mongoose = require('mongoose')

mongoose.set('bufferCommands', false)
mongoose.set('useFindAndModify', false)
mongoose.connect(`mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`, { useNewUrlParser: true, connectTimeoutMS: 2000 })

const db = mongoose.connection
db.on('connecting', () => {
  console.log('connecting to mongodb server...')
})
db.on('error', () => {
  console.error('ERRRRORROR!')
})
db.once('open', () => {
  console.log('connected')
})


module.exports = { db, mongoose }
